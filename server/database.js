var Sequelize = require('sequelize');
var config = require("./config/index");

console.log(config.mysql);
var database = new Sequelize(config.mysql, {
    pool: {
        max: 2,
        min: 1,
        idle: 10000
    }
});

var User = require("./api/user/user.model.js")(database);
var Post = require("./api/post/post.model.js")(database);
var Comment = require("./api/comment/comment.model.js")(database);

// BEGIN: MYSQL RELATIONS

User.hasMany(Post);
Post.belongsTo(User)
Post.hasMany(Comment);
Comment.belongsTo(Post);
Comment.belongsTo(User);

// END: MYSQL RELATIONS

database
    .sync({force: config.seed})
    .then(function () {
        console.log("Database in Sync Now");
        require("./seed")();
    });

module.exports = {
    User: User,
    Post: Post,
    Comment: Comment
};

